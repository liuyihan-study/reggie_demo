package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Category;

import com.itheima.reggie.entity.Setmeal;

import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.stream.Collectors;

//套餐管理
@RestController
@Slf4j
@RequestMapping("/setmeal")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private SetmealDishService setmealDishService;
    @Autowired
    private CategoryService categoryService;
    @PostMapping
    //清楚setmealCache名称下,所有的缓存数据
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> save(@RequestBody SetmealDto setmealDto) {
        log.info("套餐信息 {}", setmealDto);
        setmealService.saveWithDish(setmealDto);
        return R.success("新增套餐成功");
    }
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name) {
        //构造分页构造器
        Page<Setmeal> pageInfo = new Page<>(page, pageSize);
        Page<SetmealDto> setmealDtoPage = new Page<>(page,pageSize);
        //条件构造器
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //添加条件
        queryWrapper.like(name != null, Setmeal::getName, name);
        //添加排序条件
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        //执行分页
        setmealService.page(pageInfo, queryWrapper);
        //把普通元素拷贝到setmealDtoPage
        BeanUtils.copyProperties(pageInfo,setmealDtoPage,"records");
        //获取到套餐信息
        List<Setmeal> records = pageInfo.getRecords();
        //item代表着每一个套餐信息,
        List<SetmealDto> list = records.stream().map((item) -> {
            //创建加强版实体类对象
            SetmealDto setmealDto = new SetmealDto();
            //把套餐信息里面的元素拷贝给setmeaDto
            BeanUtils.copyProperties(item,setmealDto);
            //根据套餐信息获取到分类id,在根据分类id获取到分类名称
            Long categoryId = item.getCategoryId();
            Category category = categoryService.getById(categoryId);
            setmealDto.setCategoryName(category.getName());
            return setmealDto;

        }).collect(Collectors.toList());
        //把套餐信息和套餐里面的菜品信息全部赋值给加强实体类
        setmealDtoPage.setRecords(list);
        //返回加强的实体类
        return R.success(setmealDtoPage);
    }
    @DeleteMapping
    //清楚setmealCache名称下,所有的缓存数据
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> delete(@RequestParam List<Long> ids){
        log.info("删除ids:{}",ids);
        setmealService.removeWithDish(ids);
        return R.success("套餐数据删除成功");
    }
    @GetMapping("/list")
    @Cacheable(value = "setmealCache",key = "#setmeal.categoryId+'_'+ #setmeal.getStatus()")
    public R<List<Setmeal>> list(Setmeal setmeal){
        //添加条件构造器
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId()!=null,Setmeal::getCategoryId,setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null,Setmeal::getStatus,setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> list = setmealService.list(queryWrapper);
        return R.success(list);
    }
    //批量修改状态
    @PostMapping("/status/{status}")
    public R update(@RequestParam List<Long> ids){
        for (Long id : ids) {
            Setmeal setmeal = setmealService.getById(id);
            LambdaUpdateWrapper<Setmeal> wrapper = new LambdaUpdateWrapper<>();
            if (setmeal.getStatus()==1){
                wrapper.eq(Setmeal::getId,id).set(Setmeal::getStatus,0);
            }else {
                wrapper.eq(Setmeal::getId,id).set(Setmeal::getStatus,1);
            }
            setmealService.update(setmeal, wrapper);
        }

        return R.success("修改成功");
    }
    @GetMapping("/{id}")
    public R update(@PathVariable Long id){
        log.info("传入的id是 {}",id);
        SetmealDto setmealDto = setmealService.getByIdWithDish(id);
        return R.success(setmealDto);
    }
    @PutMapping
    public R<String> saveS(@RequestBody SetmealDto setmealDto){
        log.info("需要保存的信息是{}",setmealDto);

        setmealService.updateAndSave(setmealDto);
        return R.success("保存成功");
    }
}

