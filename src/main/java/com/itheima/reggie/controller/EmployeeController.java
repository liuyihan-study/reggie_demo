package com.itheima.reggie.controller;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {
        //①. 将页面提交的密码password进行md5加密处理, 得到加密后的字符串
        String password = employee.getPassword();
        //使用工具类对密码进行加密,并把加密之后的值再重新赋值给password
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        //②. 根据页面提交的用户名username查询数据库中员工数据信息
        LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Employee::getUsername, employee.getUsername());
        //lqw.eq(Employee::getPassword,employee.getPassword());
        Employee emp = employeeService.getOne(lqw);
        //3.如果每一查询到则返回登录失败
        if (emp == null) {
            return R.error("登录失败");
        }
        //4.密码比对,如果不一致则返回登录失败结果
        if (!emp.getPassword().equals(password)) {
            return R.error("登录失败");
        }
        //5.查看员工状态,如果为已经禁用,则返回员工以禁用结果
        if (emp.getStatus() == 0) {
            return R.error("账号已经被禁用");
        }
        //6.登录成功,将员工id存入session并返回登陆成功的结果
        request.getSession().setAttribute("employee", emp.getId());
        return R.success(emp);
    }

    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request) {
        //清理session中保存的当前员工的id
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");
    }

    @PostMapping
    public R<String> save(HttpServletRequest request, @RequestBody Employee employee) {
        //设置初始密码为123456,则需要进行MD5加密处理
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
       // employee.setCreateTime(LocalDateTime.now());
       // employee.setUpdateTime(LocalDateTime.now());
        //获取当前用户id
       // long id = (Long) request.getSession().getAttribute("employee");
       // employee.setCreateUser(id);
       // employee.setUpdateUser(id);
        employeeService.save(employee);
        return R.success("添加新员工成功");
    }
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){
        //1.构造分页构造器
        Page pageInfo = new Page(page,pageSize);
        //2.构造条件构造器
        LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper<>();
        //3,添加过滤条件
        lqw.like(StringUtils.isNotEmpty(name),Employee::getName,name);
        //4.添加排序条件
        lqw.orderByDesc(Employee::getUpdateTime);
        //5.执行查询
        employeeService.page(pageInfo,lqw);
        return R.success(pageInfo);
    }
    @PutMapping
    public R<String> update( HttpServletRequest request,@RequestBody Employee employee){
        /*1.分析:
        启用.禁用员工账号,本质上就是一个更新操作,也就是对status状态字段进行操作
        * */
        log.info(employee.toString());
        long id = Thread.currentThread().getId();
        log.info("线程id为：{}",id);
        //Long empId = (Long)request.getSession().getAttribute("employee");
        //employee.setUpdateTime(LocalDateTime.now());
       // employee.setUpdateUser(empId);
        employeeService.updateById(employee);

        return R.success("员工信息修改成功");
    }
    //点击编辑按钮,根据id修改员工信息
    @GetMapping("/{id}")
    public R<Employee> getById(@PathVariable Long id){
        log.info("根据id查询的员工信息是:");
        Employee byId = employeeService.getById(id);
        if (byId!=null){
            return R.success(byId);
        }
        return R.error("没有查询到对应员工的信息");
    }
}
