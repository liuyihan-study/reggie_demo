package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

//菜品管理
@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {
    @Autowired
    //菜品
    private DishService dishService;
    @Autowired
    //口味
    private DishFlavorService dishFlavorService;
    //分类
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedisTemplate redisTemplate;

    //新增菜品
    @PostMapping
    @CacheEvict(value = "dishCache",allEntries = true)
    public R<String> save(@RequestBody DishDto dishDto) {
        log.info(dishDto.toString());
        dishService.saveWithFlavor(dishDto);
        return R.success("新增菜品成功");
    }
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name) {
        //构造分页构造器对象
        Page<Dish> pageInfo = new Page<>(page, pageSize);
        Page<DishDto> dtoPage = new Page<>();
        //条件构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加过滤条件
        queryWrapper.like(name != null, Dish::getName, name);
        //添加排序条件
        queryWrapper.orderByDesc(Dish::getUpdateTime);
        //执行分页查询
        dishService.page(pageInfo, queryWrapper);
        //对象之间元素的拷贝,除了records需要处理外,其他全部拷贝过来
        BeanUtils.copyProperties(pageInfo, dtoPage, "records");
        //pageinfo里面现在已经有了值
        List<Dish> records = pageInfo.getRecords();
        //item代表着dish里面的每一个菜品
        List<DishDto> list = records.stream().map((item) -> {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(item, dishDto);
            //拿到每一个菜品的分类id
            Long categoryId = item.getCategoryId();
            //根据菜品的id获取到菜品分类对象
            Category category = categoryService.getById(categoryId);
            //根据获取到的菜品分类对象获取到分类名称
            if (category != null) {
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            return dishDto;
        }).collect(Collectors.toList());
        dtoPage.setRecords(list);
        return R.success(dtoPage);
    }

    //点击修改回显菜品信息
    @GetMapping("/{id}")
    //根据id查询菜品信息和对应的口味信息
    public R<DishDto> get(@PathVariable Long id) {
        DishDto dishDto = dishService.getByIdWithFlavor(id);
        return R.success(dishDto);
    }
    //修改菜品,点击保存,保存修改休息
    @PutMapping
    @CacheEvict(value = "dishCache",allEntries = true)
    public R<String> update(@RequestBody DishDto dishDto) {
        log.info("显示菜品{}", dishDto.toString());
        dishService.updateWithFlavor(dishDto);
        return R.success("修改菜品成功");
    }
    //根据菜品分类id获取到菜品信息
   /* @GetMapping("/list")
    public R<List<Dish>> list(Dish dish){
        //条件构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加条件 分类id=?
        queryWrapper.eq(dish.getCategoryId()!=null,Dish::getCategoryId,dish.getCategoryId());
        //添加条件,状态为1,处于起售状态
        queryWrapper.eq(Dish::getStatus,1);
        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> list = dishService.list(queryWrapper);
        return R.success(list);
    }*/
    @GetMapping("/list")
    @Cacheable(value = "dishCache",key = "#dish.categoryId+'_1'")
    public R<List<DishDto>> list(Dish dish) {
        List<DishDto> dishDtoList = null;
      /*  // ObjectMapper mapper = new ObjectMapper();
        //代码改造,使用带泛型的 RedisTemplate
        //动态构造key
        String key = "dish_" + dish.getCategoryId() + "_" + dish.getStatus();
        //先从redis中获取缓存的数据
        dishDtoList = (List<DishDto>) redisTemplate.opsForValue().get(key);
        // List<DishDto> list1= mapper.readValue(s, new TypeReference<List<DishDto>>() {});
        //如果存在,直接返回,无需查询数据库
        if (dishDtoList != null) {
            return R.success(dishDtoList);
        }*/
        //构造查询条件
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(dish.getCategoryId() != null, Dish::getCategoryId, dish.getCategoryId());
        //添加条件，查询状态为1（起售状态）的菜品
        queryWrapper.eq(Dish::getStatus, 1);
        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);

        List<Dish> list = dishService.list(queryWrapper);
        dishDtoList = list.stream().map((item) -> {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(item, dishDto);

            Long categoryId = item.getCategoryId();//分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            //当前菜品的id
            Long dishId = item.getId();
            LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(DishFlavor::getDishId, dishId);
            //SQL:select * from dish_flavor where dish_id = ?
            List<DishFlavor> dishFlavorList = dishFlavorService.list(lambdaQueryWrapper);
            dishDto.setFlavors(dishFlavorList);
            return dishDto;
        }).collect(Collectors.toList());
        // String jsonList = mapper.writeValueAsString(dishDtoList);
        //如果不存在,需要查询数据库,将查询到的菜品数据缓存到redis
       // redisTemplate.opsForValue().set(key, dishDtoList, 60, TimeUnit.MINUTES);
        return R.success(dishDtoList);
    }
    //Request URL: http://localhost:8080/dish/status/0?ids=1473824541800243201
    @PostMapping("/status/{ids}")
    //批量修改状态
    public R update(@RequestParam List<Long> ids) {
        log.info("id是:{}", ids);
        //log.info("信息:{}", status);
        log.info("id是:{}", ids);
        //Dish dish = dishService.getById(ids);  //
        for (Long id : ids) {
            Dish dish = dishService.getById(id);
            LambdaUpdateWrapper<Dish> wrapper = new LambdaUpdateWrapper<>();
            if (dish.getStatus()==1){
                wrapper.eq(Dish::getId,id).set(Dish::getStatus,0);
            }else {
                wrapper.eq(Dish::getId,id).set(Dish::getStatus,1);
            }
            dishService.update(dish,wrapper);
        }
        return R.success("修改成功");

    }

    //Request URL: http://localhost:8080/dish?ids=1473824541800243201
    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids) {
        log.info("要删除的是:{}", ids);
        dishService.deleteBy(ids);
        return R.success("删除成功");
    }
}
