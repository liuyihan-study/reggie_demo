package com.itheima.reggie.controller;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    //新增菜品
    @PostMapping
    public R<String> save( @RequestBody Category category){
        log.info("category:{}",category);
        categoryService.save(category);
        return R.success("新增菜品成功");
    }
    //
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize){
        //1.构造分页构造器
        Page pageInfo = new Page();
        //2.构造条件构造器
        LambdaQueryWrapper<Category> lqw = new LambdaQueryWrapper<>();
        //3.添加排序
        lqw.orderByAsc(Category::getSort);
        //4.执行分页查询
        categoryService.page(pageInfo,lqw);
        return R.success(pageInfo);
    }
    @DeleteMapping
    public R<String> delete(Long  id){
        log.info("删除:{}",id);
      //  categoryService.removeById(id);
        categoryService.remove(id);
        return R.success("删除成功");
    }
    @PutMapping
    public R<String> update(@RequestBody Category category){
        log.info("修改");
        categoryService.updateById(category);
        return R.success("修改分类信息成功");
    }
    //在菜品管理里面显示菜品分类
    //根据条件查询分类数据
    @GetMapping("/list")
    public R<List<Category>> list(Category category){
        //条件构造器
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        //添加条件
        lambdaQueryWrapper.eq(category.getType()!=null,Category::getType,category.getType());
        log.info("这是什么:{}",category.getType());
        lambdaQueryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);
        List<Category> list = categoryService.list(lambdaQueryWrapper);
        return R.success(list);
    }
}
