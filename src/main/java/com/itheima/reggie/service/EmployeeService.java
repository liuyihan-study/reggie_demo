package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Employee;
//本项目的service接口,在自定义时需要继承自mybatisplus提供的service层接口的IService
//这样就可以直接调用父接口的方法,直接执行业务操作,简化业务层代码实现
public interface EmployeeService extends IService< Employee> {
}
