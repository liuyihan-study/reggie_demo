package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper,Dish> implements DishService {

    @Autowired
    private DishFlavorService dishFlavorService;

    @Transactional//开启事务,同时成功或者同时失败
    public void saveWithFlavor(DishDto dishDto) {
        //保存菜品的基本信息到dish表中
        this.save(dishDto);
        //dishFlavor在封装数据的时候只封装了 name 和value 并没有封装dishid,没有dishid就没有办法对应的菜品
        //所以要设置id,而菜品的基本信息里面已经有了dishid,
        Long dishId = dishDto.getId();//菜品id
        //获取到菜品口味集合
        List<DishFlavor> flavors = dishDto.getFlavors();
        //item就是flvors遍历的每一个口味,把dishid给赋值进去,就可以根据dishid找到菜品
        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());

        //把菜品和口味的数据保存到flavors集合里面
        dishFlavorService.saveBatch(flavors);
    }

    /**
     * 根据id查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    public DishDto getByIdWithFlavor(Long id) {
        //根据id获取菜品信息
        Dish dish = this.getById(id);
     /*
     * 因为现在是要操作两张表 dish菜品表和DishFlavor口味表,回显数据的时候既要回显菜品信息,
     * 还要把口味信息也给回显出来
     * 在DishFlavor表中根据dish获取到dishId,然后再dishid获取到口味信息,
     * dishdto是dish表的子类,用工具类吧dish表中的信息拷贝到dishdto中,
     * 然后使用dishDto.setFlavors(list);list就是获取到的口味信息,
     * 最后返回dishsdto对象,现在dishdto对象里面已经包含了菜品信息和口味信息
     * */
        DishDto dishDto = new DishDto();
        //把菜品信息拷贝到dishdto中
        BeanUtils.copyProperties(dish, dishDto);
        //添加条件查询器
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dish.getId());
        //根据条件查询,根据id 查询到菜品的口味信息
        List<DishFlavor> list = dishFlavorService.list(queryWrapper);
        //把根据id查询到的菜品信息设置到dishDto中
        dishDto.setFlavors(list);
        //返回dishDto
        return dishDto;
    }
    @Override
    @Transactional
    public void updateWithFlavor(DishDto dishDto) {
        //获取当前dish菜品信息
        this.updateById(dishDto);
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        //添加条件,根据菜品id,获取口味信息的id,然后调用dishFlavorService删除口味信息
        queryWrapper.eq(DishFlavor::getDishId, dishDto.getId());
        dishFlavorService.remove(queryWrapper);
        //重新获取口味信息,根据dishDto.getFlavors();
        // 当点击保存的时候就是要获取当前页面的信息
        List<DishFlavor> flavors = dishDto.getFlavors();
        //根据dishDto.getid获取到菜品id,然后现在就有了口味信息,和菜品信息
        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());
        dishFlavorService.saveBatch(flavors);
    }

    @Override
    public Dish getByIdWithStatus(Long ids) {
        Dish dish = this.getById(ids);
        if (0==dish.getStatus()){
            dish.setStatus(1);
        }else{
            dish.setStatus(0);
        }
        return dish;
    }
    @Override
    @Transactional
    public void deleteBy(List<Long> ids) {
        //删除菜品,同时需要删除菜品和菜品相关的口味信息
        //1.查询菜品状态,确定是否可以删除
        //添加条件构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加条件
        queryWrapper.in(Dish::getId,ids);
        //0:表示停售状态,1表示正在售卖中
        queryWrapper.eq(Dish::getStatus,1);
        int count= this.count(queryWrapper);
        if (count>0){
            //如果不能就先抛出来一个异常
            throw new CustomException("菜品正在售卖中,不可以删除");
        }
        //如果可以删除,先删除表中数据表
        this.removeByIds(ids);
       /* //添加条件构造器
        LambdaQueryWrapper<DishFlavor> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(DishFlavor::getDishId,ids);
        dishFlavorService.remove(wrapper);*/
    }
}
