package com.itheima.reggie.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;

import com.itheima.reggie.mapper.SetmealMapper;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
    @Autowired
    private  SetmealDishService setmealDishService;
    @Override
    @Transactional
  /*  //同时要保存套餐信息和相关联的菜品信息
    具体逻辑:
    A. 保存套餐基本信息
    B. 获取套餐关联的菜品集合，并为集合中的每一个元素赋值套餐ID(setmealId)
    C. 批量保存套餐关联的菜品集合
    代码实现:*/
    public void saveWithDish(SetmealDto setmealDto) {
        //保存套餐的基本信息(输入的套餐的基本信息)
        this.save(setmealDto);
        //获取到套餐所关联的菜品,根据套餐中菜品id
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //遍历每一个菜品,
        setmealDishes  = setmealDishes.stream().map((item)->{
            //获取到菜品id,根据菜品id获取到套餐id
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);
    }

    @Override
    @Transactional
    public void removeWithDish(List<Long> ids) {
        //删除套餐,同时需要删除套餐和菜品相关联的数据
        //查询套餐状态,确定是否可用删除
        LambdaQueryWrapper<Setmeal>  queryWrapper = new LambdaQueryWrapper<>();
        //添加条件
        queryWrapper.in(Setmeal::getId,ids);
        queryWrapper.eq(Setmeal::getStatus,1);
        int count = this.count(queryWrapper);
        if (count>0){
            //如果不能删除,就抛出来 一个异常
            throw new CustomException("套餐正在寿买中,无法进行删除操作");
        }
        //如果可以删除,先删除表中的数据
        this.removeByIds(ids);
        LambdaQueryWrapper<SetmealDish> queryWrapper1 = new LambdaQueryWrapper<>();
        //添加条件
        queryWrapper1.in(SetmealDish::getSetmealId, ids);
        //删除关系表中的数据----setmeal_dish
        setmealDishService.remove(queryWrapper1);
    }
//修改  回显数据
    @Override
    public SetmealDto getByIdWithDish(Long id) {
        //获取到套餐信息,还要获取到套餐所关联的菜品信息
        Setmeal setmeal = this.getById(id);
        //创建setmeal的加强对象
        SetmealDto setmealDto = new SetmealDto();
        //把套餐的基本信息添加到加强对象中setmealDto
        BeanUtils.copyProperties(setmeal,setmealDto);
        //添加条件构造器
        LambdaQueryWrapper<SetmealDish> queryWrapper  = new LambdaQueryWrapper<>();
        //添加条件
        queryWrapper.eq(SetmealDish::getDishId,setmeal.getId());
        List<SetmealDish> setmealDishes = setmealDishService.list(queryWrapper);
        setmealDto.setSetmealDishes(setmealDishes);
        return setmealDto;
    }

    @Override
    @Transactional
    public void updateAndSave(SetmealDto setmealDto) {
        //修改套餐并保存
        //this.save(setmealDto);
       this.updateById(setmealDto);
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,setmealDto.getId());
        setmealDishService.remove(queryWrapper);
        Long id = setmealDto.getId();//套餐分类id
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes = setmealDishes.stream().map((item)->{
            item.setSetmealId(id);
            return item;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);
    }
    }

