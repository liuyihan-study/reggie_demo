package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
//在没有mybatisplus中 ,自定义的mapper接口,需要继承自BaseMapper
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee>{
}
