package com.itheima.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;

@WebFilter(filterName = "loginCheckFilter",urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {
    //路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //进行强转一下
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        //1.获取本次请求的URL
        String requestURI = request.getRequestURI();
        log.info("拦截到请求：{}",requestURI);

        //定义不需要处理的请求路径
        String[] urls = new String[]{
                "/employee/login",//登录页面不需要拦截
                "/employee/logout",//退出登录页面不需要拦截
                "/backend/**",//静态页面不需要没有数据不需要拦截,不拦截只可以看到页面,看不到数据
                "/front/**",//静态页面不需要拦截,
                "/common/**",
                "/user/sendMsg",
                "/user/login"

        };

       // filterChain.doFilter(request, response);
        //2.判断本次请求是否需要处理
        boolean check = check(urls, requestURI);
        //3.如果不需要处理,则直接放行
        if (check){
            log.info("本次请求{}不需要处理",requestURI);
            //判断check是否为true  是true直接放行
            filterChain.doFilter(request,response);
            return;
        }
        //4-1.判断登录状态,如果已经登录,则直接放行
        if(request.getSession().getAttribute("employee")!=null){
            log.info("用户已经登录,用户id为:{}",request.getSession().getAttribute("employee"));
            //判定用户是否登录,如果用户登录,再放行之前,获取httpSession中登录的用户信息
            //调用BaseContext的setCurrentId方法将当前用户id存入
            Long empId = (Long) request.getSession().getAttribute("employee");
            BaseContext.setCurrentId(empId);
            filterChain.doFilter(request,response);
            return;
        }
        //4-2判断登录状态,如果已经登录,则直接放行
        if (request.getSession().getAttribute("user")!=null){
            log.info("用户已经登录,用户id:{}",request.getSession().getAttribute("user"));
            //从页面上获取用户信息
            Long userId = (Long) request.getSession().getAttribute("user");
            //创建用户线程id变量,用户自动填充
            BaseContext.setCurrentId(userId);
            filterChain.doFilter(request,response);
            return;
        }
        log.info("用户未登录");
        //5.如果未登录则返回未登录结果,通过输出流方式向客户端页面响应数据
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return;
    }
    /**
     * 路径匹配，检查本次请求是否需要放行
     * @param urls
     * @param requestURI
     * @return
     */
    public boolean check(String[]  urls,String requestURI){
        for (String url : urls) {
            //判断获取到的路径是否属于可以放行的路径
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match){
                return true;
            }
        }
        return false;
    }

}
