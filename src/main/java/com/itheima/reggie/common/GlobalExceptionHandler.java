package com.itheima.reggie.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLIntegrityConstraintViolationException;

@Slf4j
@ResponseBody
@ControllerAdvice(annotations = {RestController.class, Controller.class})
public class GlobalExceptionHandler {
    //异常处理
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R<String> exceptionHandler(SQLIntegrityConstraintViolationException ex) {
        log.info(ex.getMessage());
        //判断错误信息里面是否包含"Duplicate entry"错误原因,使用空格将其分割成多个字符串,
        //获取到索引2 的字符串就是用户名错误的字符串
        //java.sql.SQLIntegrityConstraintViolationException: Duplicate entry 'zhangsan' for key 'idx_username'
        if (ex.getMessage().contains("Duplicate entry")) {
            //拦截错误信息,用空格将错误信息分割开,储存在一个数组里面,错误信息数组里面的索引2就是错误存在的原因
            String[] split = ex.getMessage().split(" ");
            String msg = split[2] + "已存在,请不要重新输入信息";
            return R.error(msg);
        }
        return R.error("未知错误");

    }
    //自定义异常捕获
    @ExceptionHandler(CustomException.class)
    public R<String> exceptionHandler(CustomException exception){
        log.error(exception.getMessage());
        return R.error(exception.getMessage());
    }

}
