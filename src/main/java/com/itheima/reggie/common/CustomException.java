package com.itheima.reggie.common;
//自定义异常分类
/*
* 在业务逻辑操作给偶成中,如果遇到一些业务参数,操作异常的情况下,我们直接抛出异常*/
public class CustomException extends RuntimeException {
    public CustomException(String message){
        super(message);
    }
}
